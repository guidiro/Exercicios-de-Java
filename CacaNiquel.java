
/**
 * Crie um programa que simula o funcionamento de um caça níquel, da seguinte forma:
 * 
 * 1- Sorteie 3x um valor dentre as strings do vetor de valores
 * 2- Compare os valores sorteados e determine os pontos obtidos usando o
 * seguinte critério:
 * 
 * Caso hajam três valores diferentes: 0 pontos
 * Caso hajam dois valores iguais: 100 pontos
 * Caso hajam três valores iguais: 1000 pontos
 * Caso hajam três valores "7": 5000 pontos
 * 
 */
public class CacaNiquel {
	public static void main(String args[]) {
		
		int valorPremio = 0;
		
		
		String[] valores = {"abacaxi", "framboesa", "banana", "pera", "7"};
		
		
		double cacaNiquel1 = Math.random();
		cacaNiquel1= Math.floor(cacaNiquel1 * 5);
		
		double cacaNiquel2 = Math.random();
		cacaNiquel2= Math.floor(cacaNiquel2 * 5);
		
		double cacaNiquel3 = Math.random();
		cacaNiquel3= Math.floor(cacaNiquel3 * 5);
		
		if(cacaNiquel1 == cacaNiquel2 && cacaNiquel1 == cacaNiquel3) {
			if(cacaNiquel1 == 4) {
				valorPremio = 5000;
			}
			else {
				valorPremio = 1000;
			}
		}
		else if(cacaNiquel1 == cacaNiquel2 || cacaNiquel1 == cacaNiquel3 || cacaNiquel2 == cacaNiquel3 ) {
			valorPremio = 100;
		}
		else {
			valorPremio = 0;
		}
		
		if(valorPremio > 0) {
			System.out.println("Acerrrrrtou: " + valorPremio);
		}
		else {
			System.out.println("Errrrrrroou!");
		}
		
		System.out.println("Resultado do sorteio: " + valores[(int) cacaNiquel1] + ", " + valores[(int) cacaNiquel2] + ", " + valores[(int) cacaNiquel3]);
	}
}
