import java.util.HashSet;

public class Bingo {

	/**
	 * Crie um programa que realiza o sorteio de 20 números
	 * de 1 à 50, de forma que um número sorteado não pode ser repetido.
	 * 
	 * Após o sorteio, o progama deve conferir uma cartela pré-determinada
	 * e determinar o resultado dentre as possíveis opções:
	 * 
	 * - Bingo: cartela cheia
	 * - Linha: linha cheia (vertical ou horizontal)
	 * - Erro: nenhuma das anteriores
	 *
	 * O programa deve imprimir os números sorteados e o resultado.	
	/*
	 */
	
	public static void main(String args[]) {
		int[][] cartela = {
			{10, 21, 34, 43},
			{7, 15, 11, 50},
			{41, 9, 33, 2},
			{1, 2, 34, 49}
		};
		
		HashSet<Double> vetorBingo = new HashSet<Double>();
		
		double bingo1; 
		
		while(vetorBingo.size() < 20) {
			bingo1 = Math.random();
			bingo1 = Math.ceil(bingo1 * 50);
			if(!vetorBingo.contains(bingo1)) {
				vetorBingo.add(bingo1);
			}
		}	
	
		System.out.println(vetorBingo);
		
		
		
		
		
		
	}
}